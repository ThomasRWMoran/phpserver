#!/usr/bin/env python3
import boto3
import time
import urllib.request
import instance

def wait():
    print("Going to wait for 2 mins")
    time.sleep(120)
    print("Okay let's go")

#print(instance.prov())
ids = instance.create("phpServer")
wait()
image_id = instance.webAndImage(ids[0], "phpImage")
